# Simple Spring-Boot REST API

This is a demonstration project of a very simple spring-boot REST API.

This project is designed to illustrate certain elements of a spring-boot project, and therefore has been kept simple in certain areas.


## Logging

See EmployeeController.java for an example of including proper logging in your java files.

In general you will have a class attribute similar to the following:\

```private static final Logger LOG = LoggerFactory.getLogger(EmployeeController.class);```

Where ```EmployeeController.class``` should be replaced with the name of whatever class you are currently in.

Make sure you use the org.slf4j imports as there will be other options also available.

Examples of making an actual logging call i.e. log a message to console or log file:
```
LOG.debug("My message");
LOG.info("My Message")
```

## How to include Swagger
Include a swagger UI for your REST API by including the springfox-boot-starter dependency build.gradle:
```
	implementation 'io.springfox:springfox-boot-starter:3.0.0'
```

The swagger UI will be available for your app by default at the url: /swagger-ui/
e.g. ```http://localhost:8080/swagger-ui/```

## How to Add Jacoco Code Coverage
Include code coverage reports in your app by including the Jacoco sections in build.gradle:

(1) Include the jacoco plugin line in the plugins section:
```
plugins {
	id 'org.springframework.boot' version '2.3.3.RELEASE'
	id 'io.spring.dependency-management' version '1.0.10.RELEASE'
	id 'java'
	id 'jacoco'
}
```

(2) a jacoco section
```
jacoco {
    toolVersion = "0.8.5"
    reportsDir = file("$buildDir/jacoco")
}
```

(3) a jacoco test report section:
```
jacocoTestReport {
    dependsOn test
}
```

(4) Add the finalizedBy line below to the 'test' section:
```
test {
	useJUnitPlatform()

	finalizedBy jacocoTestReport
}
```

A gradle test run should then create code coverage reports in ```build/jacoco/test```

* Running gradle test from VSCode:
![Gradle Test](https://www.benivade.com/neueda-training/Tech2020/jacoco/gradle_test_highlighted.PNG)


* Jacoco report location in VSCode:
![Gradle Jacoco](https://www.benivade.com/neueda-training/Tech2020/jacoco/gradle_jacoco.PNG)

## How to include in-memory MongoDB for Testing

The "flapdoodle" test MongoDB can be added to the project by adding the following dependency to ```build.gradle```:

```
testImplementation 'de.flapdoodle.embed:de.flapdoodle.embed.mongo'
```

This is very useful to facilitate integration testing from your project. However note that it does have some consequences such as making the tests slower to run.